const path = require('path');
const webpack = require('webpack');
const uglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = (env, argv) => {
    return {
        context: __dirname,
        devtool: argv.mode === 'development' ? 'source-map' : '',
        entry: {
            main: path.resolve(__dirname, './src/main'),
        },
        output: {
            path: path.resolve(__dirname + '/dist'),
            publicPath: '/dist/',
            filename: '[name].js'
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "[name].css"
            }),
            new OptimizeCssAssetsPlugin({
                cssProcessorOptions: {discardComments: {removeAll: true}},
                canPrint: true
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            })
        ],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        /*"style-loader",*/
                        {loader: "css-loader", options: {importLoaders: 1}},
                        "postcss-loader"
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        /*"style-loader",*/
                        {loader: 'css-loader', options: {importLoaders: 1}},
                        "postcss-loader",
                        "sass-loader",
                        {
                            loader: 'sass-resources-loader',
                            options: {
                                sourceMap: argv.mode === 'development',
                                resources: [
                                    './src/styles/vars.scss',
                                    './src/styles/mixins.scss'
                                ]
                            }
                        }
                    ]
                },
                /*{
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    loader: 'url-loader'
                },*/
                {
                    test: /\.(ttf|eot|svg|png|jpg|gif|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: 'file-loader'
                }
            ]
        },
        resolve: {
            alias: {
                legacy:  __dirname + '/assets/legacy',
                fonts:  __dirname + '/assets/fonts',
            }
        },
        devServer: {
            historyApiFallback: true,
            noInfo: true,
            stats: 'errors-only'
        },
        optimization: {
            minimizer: argv.mode === 'development' ? [] :
                [
                    /*new uglifyJSPlugin({
                        cache: true,
                        parallel: true,
                        uglifyOptions: {
                            warnings: false,
                            compress: {
                                drop_console: true,
                                unsafe: true
                            },
                            output: {
                                comments: false
                            }
                        }
                    }),*/
                    new OptimizeCssAssetsPlugin({})
                ]
        }
    }
};
