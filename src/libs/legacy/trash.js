jQuery(window).bind("load resize scroll", function (e) {
    var m_offset;
    var w = jQuery(window).width();
    if (w > 0)
        m_offset = 700;

    if (w > 600)
        m_offset = 500;

    if (w > 1000)
        m_offset = 400;

    if (w > 1440)
        m_offset = 300;

    var y = jQuery(window).scrollTop();
    var x = jQuery("#block360_7").offset().top - jQuery(window).height() - y;

    if (jQuery("#block360_7").offset().top < (y + jQuery(window).height()) &&
        jQuery("#block360_7").offset().top + jQuery("#block360_7").height() > y) {
        jQuery("#block360_7").css('background-position-x', +parseInt(x / 3) + 'px');

    }


    if (w > 767) {
        var z = jQuery("#block360_5").offset().top + m_offset - jQuery(window).height() - y;

        if (jQuery("#block360_5").offset().top + m_offset < (y + jQuery(window).height()) &&
            jQuery("#block360_5").offset().top + jQuery("#block360_5").height() > y) {
            jQuery("#block360_5").css('background-position-y', +parseInt((z) / 2) + 'px');
        }
    }


});

jQuery(document).ready(function () {
    var y = jQuery(window).scrollTop();
    var z = jQuery("#block360_7").offset().top + jQuery("#block360_7").height();
    var x = jQuery("#block360_7").offset().top - jQuery(window).height() - z;

    if (jQuery("#block360_7").offset().top + jQuery("#block360_7").height() < y) {
        jQuery("#block360_7").css('background-position-x', parseInt(x / 3) + 'px');

    }
    var w = jQuery(window).width();
    if (w > 767) {
        var y2 = jQuery(window).scrollTop();
        var z2 = jQuery("#block360_5").offset().top + jQuery("#block360_5").height();
        var x2 = jQuery("#block360_5").offset().top - jQuery(window).height() - z2;

        if (jQuery("#block360_5").offset().top + jQuery("#block360_5").height() < y2) {
            jQuery("#block360_5").css('background-position-y', parseInt(x2 / 2) + 'px');

        }
    }

    jQuery('[data-fancybox="amazon-popups"]').fancybox({
        toolbar: true,
        smallBtn: false,
    });

    jQuery('[data-fancybox="instagram-popups"]').fancybox({
        toolbar: true,
        smallBtn: false,
    });

    jQuery(window).scroll(function () {
        var windowHeight = $(window).height();
        var s = $(window).scrollTop();
        var s1 = $('#dc1.owl-carousel .owl-stage');
        var s2 = $('#dc2.owl-carousel .owl-stage');
        var delta = ($(document).scrollTop() - s1.offset().top) % windowHeight / 4;
        jQuery('#dc2.owl-carousel .owl-stage').css("margin-left", delta);
        jQuery('#dc1.owl-carousel .owl-stage').css("margin-left", (-delta));
    });

    jQuery('#block2 .item-list').matchHeight();
    jQuery('#block2 .item').matchHeight();

    jQuery('#block3 .item').matchHeight();

    jQuery('#block9 .item').matchHeight();
    jQuery('#block10 .item').matchHeight();
    jQuery('#block13 .item-content').matchHeight();
    jQuery('#block13 .item').matchHeight();
    jQuery('#block14 .item').matchHeight();

    var dc1 = jQuery('#dc1');
    var dc2 = jQuery('#dc2');
    var owl2 = jQuery('.press-block');
    var owl3 = jQuery('.amazon-carousel');
    var owl4 = jQuery('.instagram-carousel');

    dc1.owlCarousel({
        loop: true,
        margin: 20,
        nav: false,
        dots: false,
        mouseDrag: true,
        touchDrag: true,
        lazyLoad: true,
        autoWidth: true,
        //autoplay:true,
        //autoplayTimeout:4000,
        //autoplayHoverPause:true,
        navText: ['', ''],
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 3
            },
            1080: {
                items: 5
            }
        }
    });
    dc2.owlCarousel({
        loop: true,
        margin: 20,
        nav: false,
        dots: false,
        mouseDrag: true,
        touchDrag: true,
        lazyLoad: true,
        autoWidth: true,
        //autoplay:true,
        //autoplayTimeout:4000,
        //autoplayHoverPause:true,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1,
                margin: 10,
            },
            560: {
                items: 2,
                margin: 20,
            },
            850: {
                items: 3,
                margin: 20,
            },
        }
    });
    owl2.owlCarousel({
        margin: 20,
        loop: true,
        nav: false,
        dots: false,
        mouseDrag: true,
        touchDrag: true,
        lazyLoad: false,
        autoWidth: true,
        navText: ['', ''],
        responsive: {
            0: {
                items: 4
            },
        }
    });

    owl3.owlCarousel({
        loop: false,
        margin: 20,
        nav: true,
        dots: false,
        mouseDrag: true,
        touchDrag: true,
        lazyLoad: true,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1,
                margin: 10,
            },
            560: {
                items: 2,
                margin: 20,
            },
            850: {
                items: 3,
                margin: 20,
            },
        }
    });
    owl4.owlCarousel({
        loop: false,
        rewind: true,
        margin: 20,
        nav: true,
        dots: false,
        mouseDrag: true,
        touchDrag: true,
        lazyLoad: true,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1,
                margin: 10,
            },
            560: {
                items: 2,
                margin: 20,
            },
            850: {
                items: 3,
                margin: 20,
            },
        }
    });
});

jQuery(document).ready(function () {
    // ANCHOR SMOOTH SCROLL
    $('a.scrl')
        .click(function (event) {
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    event.preventDefault();

                    $('html, body').animate({
                        scrollTop: (target.offset().top - 90)
                    }, 1000, function () {
                    });
                }
            }
        });

    $('a.scrl2')
        .click(function (event) {
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    event.preventDefault();

                    $('html, body').animate({
                        scrollTop: (target.offset().top - 160)
                    }, 1000, function () {
                    });
                }
            }
        });


    $('.logo-carousel').owlCarousel({
        loop: true,
        margin: 20,
        nav: true,
        dots: true,
        mouseDrag: true,
        touchDrag: true,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1
            },
            320: {
                items: 2
            },
            700: {
                items: 3
            },
            1080: {
                items: 4
            }
        }
    });

    $("[data-tooltip]").mousemove(function (eventObject) {

        $data_tooltip = $(this).attr("data-tooltip");

        $("#mavd-tooltip").text($data_tooltip)
            .css({
                "top": eventObject.pageY + 5,
                "right": $(document).width() - eventObject.pageX
            })
            .show();

    }).mouseout(function () {

        $("#mavd-tooltip").hide()
            .text("")
            .css({
                "top": 0,
                "right": 0
            });
    });

    jQuery('#callback-phone').on('keyup change paste', function () {
        jQuery('#callback-phone-block').removeClass('error');
    });
    jQuery('#callback-name').on('keyup change paste', function () {
        jQuery('#callback-name-block').removeClass('error');
    });
    jQuery('#subscribe-email').on('keyup change paste', function () {
        jQuery('#subscribe-email-block').removeClass('error');
        jQuery('#subscribe-email-block').removeClass('is-valid');
    })

    jQuery('#callback-submit').click(function () {
        if (jQuery('#callback-phone').val() == '')
            jQuery('#callback-phone-block').addClass('error');
        if (jQuery('#callback-name').val() == '')
            jQuery('#callback-name-block').addClass('error');

        if (jQuery('#callback-phone-block').hasClass('error') || jQuery('#callback-name-block').hasClass('error'))
            return;

        jQuery('#callback-phone-block').hide();
        jQuery('#callback-name-block').hide();
        jQuery('.modal-caption').hide();
        jQuery('.modal-footer').hide();
        jQuery('#callback-submit-block').html(jQuery('#callback-name').val() + ', Ваша заявка принята, мы перезвоним вам в ближайшее время.');

        jQuery.ajax({
            method: "POST",
            url: "/wp-content/plugins/ra-callback-popup/ajax/ajax-new.php",
            data: {
                site: 1,
                phone: jQuery('#callback-phone').val(),
                name: jQuery('#callback-name').val(),
            }
        })
            .done(function (msg) {
                if (msg['result']) {
                }
                else {
                }
            });

    });

    jQuery('#subscribe-submit').click(function () {
        if (jQuery('#subscribe-email').val() == '')
            jQuery('#subscribe-email-block').addClass('error');

        if (jQuery('#subscribe-email-block').hasClass('error'))
            return;

        jQuery.ajax({
            method: "POST",
            url: "/wp-content/plugins/ra-mailchimp/ajax/new-subscribe.php",
            data: {
                site: 1,
                email: jQuery('#subscribe-email').val(),
            }
        })
            .done(function (msg) {
                if (msg['ok']) {
                    jQuery('#subscribe-submit').hide();
                    jQuery('#subscribe-email-block').addClass('is-valid');
                }
                else {
                    jQuery('#subscribe-email-block').addClass('error');
                }
            });

    });


    // NEW HEADER
    jQuery('.burger, .overlay').click(function () {
        var mobileHeaderTransform = window.innerWidth - 56; // WINDOW WIDTH - BUTTON SIZE

        // IF OPEN
        if (jQuery(this).hasClass('clicked')) {
            jQuery.scrollLock();
            jQuery('.desktop-header').css('transform', 'translateX(0)');
            jQuery('.mobile-header').removeClass('mobile-header-show');
            jQuery('.burger').removeClass('clicked');
            jQuery('.overlay').removeClass('show');
            jQuery('nav').removeClass('show');
            jQuery('body').removeClass('overflow');
        } else { // IF CLOSED
            jQuery.scrollLock('enable');
            jQuery('.desktop-header').css('transform', 'translateX(' + mobileHeaderTransform + 'px)');
            jQuery('.mobile-header').addClass('mobile-header-show');
            jQuery('.burger').addClass('clicked');
            jQuery('.overlay').addClass('show');
            jQuery('nav').addClass('show');
            jQuery('body').addClass('overflow');
        }

    });

    jQuery('.overlay').click(function () {
        jQuery.scrollLock();
        jQuery('.desktop-header').css('transform', 'translateX(0)');
        jQuery('.mobile-header').removeClass('mobile-header-show');
        jQuery('.burger').removeClass('clicked');
        jQuery('.overlay').removeClass('show');
        jQuery('nav').removeClass('show');
        jQuery('body').removeClass('overflow');
    });


    // MOBILE NAVIGATION
    jQuery('.collapsible .mobile-navigation-list-title').click(function () {
        var parnt = jQuery(this).parent();
        var countListItems = parnt.children('.inner-navigation-list').children().length;
        var listItemHeight = parnt.children('.inner-navigation-list').children('.inner-navigation-list-item').height();
        if (jQuery(this).parent().hasClass('product-card-list'))
            listItemHeight = listItemHeight + 15;
        var listHeight = countListItems * listItemHeight;

        if (parnt.hasClass('active')) {
            parnt.removeClass('active');
            parnt.children('.inner-navigation-list').css('height', '0');
        } else {
            parnt.addClass('active');
            parnt.children('.inner-navigation-list').css('height', listHeight + "px");
        }

    });


    function isOutside(e, parent) {
        var relatedTarget = e.relatedTarget;
        if (!relatedTarget) {
            relatedTarget = (e.type == 'mouseout') ? e.toElement : e.fromElement;
        }
        while (relatedTarget && relatedTarget !== parent) {
            relatedTarget = relatedTarget.parentNode;
        }
        return !relatedTarget;
    }

    var top_menu = document.getElementById("mmenu6014");
    var bottom_menu = document.getElementById("drawer6014");

    var timer_top_menu;
    var timer_bottom_menu;

    function makeVisible(elem) {
        jQuery('#mmenu6014').addClass('active');
        jQuery('#drawer6014').addClass('show-drawer');
    }

    top_menu.onmouseover = function (e) {
        e = e || event;
        if (!isOutside(e, top_menu) || !isOutside(e, bottom_menu)) return;
        clearTimeout(timer_bottom_menu);
        timer_top_menu = setTimeout(function () {
            makeVisible(bottom_menu)
        }, 250);
    }

    top_menu.onmouseout = function (e) {
        e = e || event;
        if (!isOutside(e, top_menu) || !isOutside(e, bottom_menu)) return; // отсечь событие

        timer_bottom_menu = setTimeout(function (e) {
            clearTimeout(timer_top_menu);
            jQuery('#mmenu6014').removeClass('active');
            jQuery('#drawer6014').removeClass('show-drawer');
        }, 400);

    }

    bottom_menu.onmouseover = function (e) {
        e = e || event;
        clearTimeout(timer_bottom_menu);
    }

    bottom_menu.onmouseout = function (e) {
        e = e || event;
        if (!isOutside(e, this) || !isOutside(e, top_menu)) return; // отсечь событие
        clearTimeout(timer_top_menu);
        timer_bottom_menu = setTimeout(function (e) {
            jQuery('#mmenu6014').removeClass('active');
            jQuery('#drawer6014').removeClass('show-drawer');
        }, 200);

    }


    /*
        var top_menu2 = document.getElementById("mmenu6015");
        var timer_top_menu2;
        top_menu2.onmouseover = function(e){
            clearTimeout(timer_top_menu3);
            clearTimeout(timer_top_menu2);
            e = e || event;
            if (!isOutside(e, this)) return; // отсечь событие
        }
    */
//////////////////////////////////////////////////////
    var top_menu2 = document.getElementById("mmenu6017");
    var bottom_menu2 = document.getElementById("drawer6017");

    var timer_top_menu2;
    var timer_bottom_menu2;

    function makeVisible2(elem) {
        jQuery('#mmenu6017').addClass('active');
        jQuery('#drawer6017').addClass('show-drawer');
    }

    top_menu2.onmouseover = function (e) {
        console.log('onmouseover');
        e = e || event;
        if (!isOutside(e, top_menu2) || !isOutside(e, bottom_menu2)) return;
        clearTimeout(timer_bottom_menu2);
        timer_top_menu2 = setTimeout(function () {
            makeVisible2(bottom_menu2)
        }, 250);
    }

    top_menu2.onmouseout = function (e) {
        console.log('onmouseout');
        e = e || event;
        if (!isOutside(e, top_menu2) || !isOutside(e, bottom_menu2)) return; // отсечь событие

        timer_bottom_menu2 = setTimeout(function (e) {
            clearTimeout(timer_top_menu2);
            jQuery('#mmenu6017').removeClass('active');
            jQuery('#drawer6017').removeClass('show-drawer');
        }, 400);

    }

    bottom_menu2.onmouseover = function (e) {
        e = e || event;
        clearTimeout(timer_bottom_menu2);
    }

    bottom_menu2.onmouseout = function (e) {
        e = e || event;
        if (!isOutside(e, this) || !isOutside(e, top_menu2)) return; // отсечь событие
        clearTimeout(timer_top_menu2);
        timer_bottom_menu2 = setTimeout(function (e) {
            jQuery('#mmenu6017').removeClass('active');
            jQuery('#drawer6017').removeClass('show-drawer');
        }, 200);

    }
//////////////////////////////////////////////////////
    if (document.getElementById('mmenu6015')) {
        var top_menu3 = document.getElementById("mmenu6015");
        var timer_top_menu3;
        top_menu3.onmouseover = function (e) {
            clearTimeout(timer_top_menu3);
            clearTimeout(timer_top_menu2);
            e = e || event;
            if (!isOutside(e, this)) return; // отсечь событие
        }
    }


    // FILE END
});
