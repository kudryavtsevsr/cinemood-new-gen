window.$ = window.jQuery = require('jquery');

// require('./plugins/fancybox2/jquery.fancybox.min');
// require('./plugins/fancybox2/jquery.fancybox.min.css');
//
require('./plugins/matchHeight/jquery.matchHeight-min');
//
require('./plugins/owlcarousel/owl.carousel.min');
require('./plugins/owlcarousel/assets/owl.carousel.min.css');
//require('./plugins/owlcarousel/assets/owl.theme.default.css');
//
// require('./plugins/scrollLock/jquery.scrollLock');
//
// require('./plugins/swiper/swiper.min');
// require('./plugins/swiper/swiper.min.css');
//
// require('./plugins/lazysizes.min');
//
require('./plugins/modernizr-custom');
//
// require('./plugins/ScrollMagic.min');

jQuery(document).ready(
    function () {
        jQuery(window).scroll(function () {
            var windowHeight = $(window).height();
            var s = $(window).scrollTop();
            var s1 = $('#dc1.owl-carousel .owl-stage');
            var s2 = $('#dc2.owl-carousel .owl-stage');
            var delta = ($(document).scrollTop() - s1.offset().top) % windowHeight / 4;
            jQuery('#dc2.owl-carousel .owl-stage').css("margin-left", delta);
            jQuery('#dc1.owl-carousel .owl-stage').css("margin-left", (-delta));
        });

        var dc1 = jQuery('#dc1');
        var dc2 = jQuery('#dc2');
        var owl2 = jQuery('.press-block');
        var owl3 = jQuery('.amazon-carousel');
        var owl4 = jQuery('.instagram-carousel');

        dc1.owlCarousel({
            loop: true,
            margin: 20,
            nav: false,
            dots: false,
            mouseDrag: true,
            touchDrag: true,
            lazyLoad: true,
            autoWidth: true,
            //autoplay:true,
            //autoplayTimeout:4000,
            //autoplayHoverPause:true,
            navText: ['', ''],
            responsive: {
                0: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1080: {
                    items: 5
                }
            }
        });
        dc2.owlCarousel({
            loop: true,
            margin: 20,
            nav: false,
            dots: false,
            mouseDrag: true,
            touchDrag: true,
            lazyLoad: true,
            autoWidth: true,
            //autoplay:true,
            //autoplayTimeout:4000,
            //autoplayHoverPause:true,
            navText: ['', ''],
            responsive: {
                0: {
                    items: 1,
                    margin: 10,
                },
                560: {
                    items: 2,
                    margin: 20,
                },
                850: {
                    items: 3,
                    margin: 20,
                },
            }
        });
        owl2.owlCarousel({
            margin: 20,
            loop: true,
            nav: false,
            dots: false,
            mouseDrag: true,
            touchDrag: true,
            lazyLoad: false,
            autoWidth: true,
            navText: ['', ''],
            responsive: {
                0: {
                    items: 4
                },
            }
        });
        owl3.owlCarousel({
            loop: false,
            margin: 20,
            nav: true,
            dots: false,
            mouseDrag: true,
            touchDrag: true,
            lazyLoad: true,
            navText: ['', ''],
            responsive: {
                0: {
                    items: 1,
                    margin: 10,
                },
                560: {
                    items: 2,
                    margin: 20,
                },
                850: {
                    items: 3,
                    margin: 20,
                },
            }
        });
        owl4.owlCarousel({
            loop: false,
            rewind: true,
            margin: 20,
            nav: true,
            dots: false,
            mouseDrag: true,
            touchDrag: true,
            lazyLoad: true,
            navText: ['', ''],
            responsive: {
                0: {
                    items: 1,
                    margin: 10,
                },
                560: {
                    items: 2,
                    margin: 20,
                },
                850: {
                    items: 3,
                    margin: 20,
                },
            }
        });

        jQuery('#block2 .item-list').matchHeight();
        jQuery('#block2 .item').matchHeight();
        jQuery('#block3 .item').matchHeight();
        jQuery('#block9 .item').matchHeight();
        jQuery('#block10 .item').matchHeight();
        jQuery('#block13 .item-content').matchHeight();
        jQuery('#block13 .item').matchHeight();
        jQuery('#block14 .item').matchHeight();

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        if (localStorage.getItem('footer-subscribe') != '' && localStorage.getItem('footer-subscribe') != null) {
            jQuery('#subscribe').val(localStorage.getItem('footer-subscribe'));
        }
        jQuery('.sub-button').on('click touch', function () {
            var email = jQuery('#subscribe').val();
            if (jQuery("#sub-foot").hasClass('done'))
                return;
            if (validateEmail(email)) {
                jQuery("#subscribe").removeClass('er');
                jQuery("#sub-foot").addClass('done');
                setTimeout(function () {
                    jQuery(".sub-button").html('Thank you!');
                }, 450);

                localStorage.setItem('footer-subscribe', email);
            } else {
                jQuery("#subscribe").addClass('er');
            }
            return false;
        });
    });
