window.$ = window.jQuery = require('jquery');

import 'normalize.css';
import './libs/legacy'
import './styles/fonts.scss';
import './styles/main.scss';

$(() => {
    const $window = $(window);
    const $document = $(document);

    // Появление кнопки при скролле
    let currentBuyContainerState;
    let lastBuyContainerState;
    const $mainOrderElement = $('.new-gen-main__order');
    const $headerBlock = $('.new-gen-header');

    $document
        .on('buyButtonNotInWindow', () => {
            $headerBlock.addClass('new-gen-header--scrolled');
        })
        .on('buyButtonInWindow', () => {
            $headerBlock.removeClass('new-gen-header--scrolled');
        });

    const checkBuyContainerPosition = () => {
        const isBuyContainerNotInWindow =
            $mainOrderElement.get(0).getBoundingClientRect().top + $mainOrderElement.get(0).offsetHeight < 0;
        if (isBuyContainerNotInWindow) {
            currentBuyContainerState = 'notInWindow';
            if (currentBuyContainerState !== lastBuyContainerState) $document.trigger('buyButtonNotInWindow');
        } else {
            currentBuyContainerState = 'inWindow';
            if (currentBuyContainerState !== lastBuyContainerState) $document.trigger('buyButtonInWindow');
        }
        lastBuyContainerState = currentBuyContainerState;
    };

    checkBuyContainerPosition();
    if ($mainOrderElement.length !== 0) {
        $window.on('scroll', () => {
            checkBuyContainerPosition();
        });
        $window.on('resize', () => {
            checkBuyContainerPosition();
        });
    }
});
